package task4

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
)

// Log Dump
type LogDump struct {
	PID     string
	SF_AT   string
	SF_TEXT string
}

// Parse parses logfile and returns dump info of drweb-se segmentation faults
func Parse(path string) ([]*LogDump, error) {

	// chans for interaction with gorutine
	data := make(chan string)
	result := make(chan []*LogDump, 1)

	// opening file
	inFile, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("can not open file: %v", err)
	}

	// running gorutine with interaction chans
	go parseLines(data, result)

	// create new scanner with splitter for scanning file
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)

	// scan whole file
	for scanner.Scan() {
		// write scanned line into chan
		s := scanner.Text()
		data <- s
	}
	// close input chan
	close(data)

	// waiting for gorutine
	dump := <-result

	// close file
	err = inFile.Close()
	if err != nil {
		return nil, fmt.Errorf("can not close file: %v", err)
	}

	return dump, nil
}

// parseLines parses lines got from data chan
func parseLines(data <-chan string, result chan<- []*LogDump) {

	// map of LogDumps
	logDumpMap := make(map[string]*LogDump)

	// loop for handeling data chan
	for {
		select {
		case d, ok := <-data:
			// if chan is closed than gorutine must be stopped
			if !ok {
				log.Println("stopped parseLines")

				// convert map to slice
				var res []*LogDump
				for _, ld := range logDumpMap {
					res = append(res, ld)
				}

				// writing dump slice into result chan
				result <- res
				// close gorutine
				return
			}

			// handle line if it contains segmentation fault
			if isSegmentationFault(d) {
				// find pname, it will be used as key for logDumpMap
				if pn, ok := findPName(d); ok {
					// skip logDump if it has already contains in map
					if _, ok := logDumpMap[pn]; ok {
						log.Printf("log dump pname=%s has already contains", pn)
						continue
					}

					// find PID and SF_AT of new dump
					pid := findPID(d)
					sfat := findSFAT(d)

					// add new logDump into map
					logDumpMap[pn] = &LogDump{
						PID:   pid,
						SF_AT: sfat,
					}
					continue
				}
			}

			// handle other lines, add SF_TEXT info into logDump
			if pname, ok := findPName(d); ok {
				if v, ok := logDumpMap[pname]; ok {
					if t, ok := findSFTEXT(d); ok {
						v.SF_TEXT += ("\n" + t)
					}
				}
			}
		}
	}
}

// isSegmentationFault returns true if string contains segmentation fault info
func isSegmentationFault(s string) bool {

	r := regexp.MustCompile("drweb-se[[:ascii:]]+\\[err][[:ascii:]]+" +
		"Segmentation fault[[:ascii:]]+$")

	return r.MatchString(s)
}

// findPID returns PID of process from segmentation fault string
func findPID(s string) string {
	r := regexp.MustCompile("\\[err][[:space:]]*:[[:space:]]*[[:alpha:]]-[0-9]+")
	s = r.FindString(s)
	r = regexp.MustCompile("[0-9]+")
	s = r.FindString(s)

	return s
}

// findSFAT returns SF_AT from segmentation fault string
func findSFAT(s string) string {
	r := regexp.MustCompile("Segmentation fault at [0-9]+")
	s = r.FindString(s)
	r = regexp.MustCompile("[0-9]+")
	s = r.FindString(s)

	return s
}

// findPName returns pname of drweb-se pnames (process name, F-14211 from example)
// also it returns true if pname was found, false in other cases
func findPName(s string) (string, bool) {
	r := regexp.MustCompile("drweb-se[[:ascii:]]+\\[err][[:space:]]*:[[:space:]]*[[:alpha:]]-[0-9]+")
	s = r.FindString(s)
	r = regexp.MustCompile("\\[err][[:space:]]*:[[:space:]]*[[:alpha:]]-[0-9]+")
	s = r.FindString(s)
	r = regexp.MustCompile("[[:alpha:]]-[0-9]+")
	s = r.FindString(s)

	if s != "" {
		return s, true
	}

	return s, false
}

// findSFTEXT returns SF_TEXT info if line s contains it
func findSFTEXT(s string) (string, bool) {

	// getting pname
	pn, ok := findPName(s)
	if !ok {
		return "", false
	}

	// first registers which can be found in string
	registers := []string{"EAX", "ESI", "ESP", "EIP"}

	// regexp for matching
	rgxpMatch := fmt.Sprintf("drweb-se[[:ascii:]]+\\[err][[:space:]]*:"+
		"[[:space:]]*%s[[:space:]]*:[[:space:]]Dump[[:space:]]*:"+
		"[[:space:]]*", pn)

	// walk through all registers
	for _, v := range registers {
		r := regexp.MustCompile(fmt.Sprintf("%s%s=", rgxpMatch, v))
		// if register found in string
		if r.MatchString(s) {
			// then get registers info from string
			r = regexp.MustCompile(fmt.Sprintf("%s=[[:ascii:]]+", v))
			s = r.FindString(s)
			return s, true
		}
	}

	return "", false
}
