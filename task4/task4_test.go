package task4

import (
	"testing"
)

// Test_Parse tests function Parse
func Test_Parse(t *testing.T) {
	parsedLog, err := Parse("./logfile.log")
	if err != nil {
		t.Errorf("Parse: %v", err)
	}

	// compare len of result slice
	if len(parsedLog) != 2 {
		t.Errorf("parsedLog len: got %d, expected 2", len(parsedLog))
	}

	// compare parsed pids
	if parsedLog[0].PID == parsedLog[1].PID {
		t.Errorf("Processes pids ars same %s, %s, expected different", parsedLog[0].PID, parsedLog[1].PID)
	}

	if parsedLog[0].PID != "14211" && parsedLog[0].PID != "14222" {
		t.Errorf("Unusual pid %v", parsedLog[0].PID)
	}

	if parsedLog[1].PID != "14211" && parsedLog[1].PID != "14222" {
		t.Errorf("Unusual pid %v", parsedLog[1].PID)
	}
}
