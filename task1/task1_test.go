package task1

import (
	"sort"
	"testing"
)

// Test_ReadDir
func Test_ReadDir(t *testing.T) {
	// testing from root dir for task1_test.go (recursion walk)
	got, err := ReadDir(".")
	if err != nil {
		t.Errorf("ReadDirWithTemplate: %v", err)
	}
	// expected result
	expected := []string{"x64_600_sspace_windows", "unix_1100_engine-e2k-linux_vdb"}

	// sorting expected and got results
	sort.Strings(got)
	sort.Strings(expected)

	// compare expected and got results
	for i := range got {
		if got[i] != expected[i] {
			t.Errorf("ReadDirWithTemplate: expected %s, got %s", expected[i], got[i])
		}
	}
}
