package task1

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

// ReadDir return unique filenames slice in directory with cutted template
// it ReadDirWithTemplate with default template
// it gets path of dir
func ReadDir(path string) ([]string, error) {

	// run ReadDirWithTemplate with default template
	dir, err := ReadDirWithTemplate("./dir1",
		"^image-", "-[0-9]{8}T[0-9]{6}.tar.gz$")
	if err != nil {
		return nil, fmt.Errorf("can not ReadDirWithTemplate: %v", err)
	}

	// returns dirs
	return dir, nil
}

// ReadDirWithTemplate return unique filenames slice in directory with cutted template
// it gets path of dir, beginning template and end template in string
func ReadDirWithTemplate(path, begTmplt, endTmplt string) ([]string, error) {

	// check templates
	r1, err := regexp.Compile(begTmplt)
	if err != nil {
		return nil, fmt.Errorf("can not parse begTmplt, %v", err)
	}
	r2, err := regexp.Compile(endTmplt)
	if err != nil {
		return nil, fmt.Errorf("can not parse endTmplt, %v", err)
	}

	// run readDir
	res, err := readDir(path, r1, r2)
	if err != nil {
		return nil, fmt.Errorf("can not readDir: %v", err)
	}

	// making results unique
	res = makeUnique(res)

	return res, nil
}

// makeUnique removes duplicates from s
func makeUnique(s []string) (res []string) {

	// map for making slice elements unique
	mp := make(map[string]string)
	for _, v := range s {
		mp[v] = v
	}

	// writing map into slice
	for _, v := range mp {
		res = append(res, v)
	}

	return res
}

// readDir gets path, and regexp 2 templates for begin and end, walks through
// all files in directory and parses appropriate files
// it returns slice of parsed filenames
func readDir(path string, begTmplt, endTmplt *regexp.Regexp) ([]string, error) {
	var result []string

	// walk through all files in dir
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}
		// skip if file is directory
		if info.IsDir() {
			return nil
		}

		// if file contains appropriate name then it name adds into result slice
		if isAppropriateName(info.Name(), begTmplt, endTmplt) {
			name := parseName(info.Name(), begTmplt, endTmplt)
			result = append(result, name)
		}

		return nil
	})

	if err != nil {
		fmt.Errorf("can not walk through dirs: %v", err)
	}

	return result, nil
}

// isAppropriateName compares full file names according to templates
func isAppropriateName(name string, begTmplt, endTmplt *regexp.Regexp) bool {
	if begTmplt.MatchString(name) && endTmplt.MatchString(name) {
		return true
	}
	return false
}

// parseName returs name by templates for beginning and ending
func parseName(name string, begTmplt, endTmplt *regexp.Regexp) string {
	name = begTmplt.ReplaceAllString(name, "")
	name = endTmplt.ReplaceAllString(name, "")

	return name
}
