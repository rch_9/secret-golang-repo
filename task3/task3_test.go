package task3

import (
	"testing"
)

// Compare count of nodes "scripts"
func Test_FetchScripts(t *testing.T) {

	// fetch slice of script metadata
	res, err := FetchScripts("https://drweb.com")
	if err != nil {
		t.Errorf("Error %v", err)
	}

	// expected len of slice
	drwebruCount := 45

	// compare got and expected
	if len(res) != drwebruCount {
		t.Errorf("Error: expexted %v, got: %v", drwebruCount, len(res))
	}
}
