package task3

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

// ElementNode type from html package
const elemTypeScript = "script"

// GetScripts returns metadata by url
func FetchScripts(url string) ([]string, error) {

	// fetching html content
	s, err := FetchHTML(url)
	if err != nil {
		return nil, fmt.Errorf("error in FetchScripts: %v", err)
	}

	// use fetched content to get html
	res, err := GetHTMLScripts(s)
	if err != nil {
		return nil, fmt.Errorf("format %v", err)
	}

	return res, nil
}

// GetHTMLScripts returns HTML scripts metadata from html (param s)
func GetHTMLScripts(s string) ([]string, error) {

	// result metadata slice
	var res []string

	// parsing html content
	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}

	// traversing through all parsed element nodes
	traverse(doc, &res)

	return res, nil
}

// FetchHTML fetches html page by it url
func FetchHTML(url string) (string, error) {
	// making http request by url
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("in GetHTML: %v", err)
	}

	// reading resp body
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("in GetHTML: %v", err)
	}

	// closing resp
	err = resp.Body.Close()
	if err != nil {
		return "", fmt.Errorf("in GetHTML: %v", err)
	}

	// casting bytes to string
	res := string(bytes)

	return res, nil
}

// traverse traverses all nodes of html and appends script content into res
func traverse(n *html.Node, res *[]string) {

	// if element is script then get script content
	if n.Type == html.ElementNode && n.Data == elemTypeScript {
		s, ok := getScriptData(n)

		// if script has content then append to res this content
		if ok {
			(*res) = append(*res, s)
		}
	}

	// traversing through all node
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		traverse(c, res)
	}
}

// getScriptData returns metadata of script node
func getScriptData(n *html.Node) (string, bool) {

	// if scrit has attr "scr" then return src value
	for _, a := range n.Attr {
		if a.Key == "src" {
			return a.Val, true
		}
	}

	// if first child (script data) not nil then return it
	if n.FirstChild != nil {
		return n.FirstChild.Data, true
	}

	// returning empty metadata
	return "", false
}
