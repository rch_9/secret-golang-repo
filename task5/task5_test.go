package task5

import (
	"testing"
)

// Test_CountJSON
func Test_CountJSON(t *testing.T) {
	// check json 1
	res55 := CountJSON("file1.json")
	if res55 != 55 {
		t.Errorf("Got %d, expected 55", res55)
	}

	// check json 2
	res125 := CountJSON("file2.json")
	if res125 != 125 {
		t.Errorf("Got %d, expected 125", res125)
	}

	res75 := CountJSON("file3.json")
	if res75 != 75 {
		t.Errorf("Got %d, expected 115", res75)
	}
}
