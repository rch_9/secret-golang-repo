package task5

import (
	"log"
	"reflect"
	"strconv"
)

// type for working with map
type jsonMap map[string]interface{}

// get returns json fields values as strings
// json.Unmarshal parses integers as float64,
// this is a main reason why i need it this method
func (m jsonMap) get(k string) (res string, ok bool) {
	val, ok := m[k]
	// if !ok {
	// 	return "", false
	// }
	switch reflect.TypeOf(val).Kind() {
	case reflect.Float64:
		res = strconv.FormatFloat(val.(float64), 'G', -1, 64)
	case reflect.Bool:
		res = strconv.FormatBool(val.(bool))
	case reflect.String:
		res = val.(string)
	default:
		// type is not float64, bool or string
		log.Fatalf("can not parse field with key %s and value %v", k, val)
	}

	return res, ok
}
