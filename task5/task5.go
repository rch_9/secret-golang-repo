package task5

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"regexp"
	"strings"
)

const (
	// json keys
	jsonKeyInfo = "info"
	jsonKeyURL  = "url"
)

// CountJSON counts json prior according to rules json
func CountJSON(filename string) (res int) {
	// reading json file
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("fatal")
	}

	// making map for store data
	m := make(map[string]interface{})

	// unmarshalling json
	err = json.Unmarshal(data, &m)
	if err != nil {
		log.Fatal(err)
	}

	// parse config file
	conf, err := ParseYaml(RulesFile)
	if err != nil {
		log.Fatal(err)
	}

	// set default value
	res = conf.Def

	// get info from json into m2
	m2 := m[jsonKeyInfo]

	// add prior by info and URL, info is a submap of all json map with url and info
	res += countInfoPrior(m2.(map[string]interface{}), *conf)
	res += countURLPrior(m[jsonKeyURL].(string), *conf)

	return res
}

// countInfoPrior returns sum info prior
func countInfoPrior(m jsonMap, conf config) (res int) {

	// loop for all fields of info config
	for _, f := range conf.Info.Fields {
		if v, ok := m.get(f.Name); ok && f.Value == v {
			res += f.Item
		}
	}

	return res
}

// countURLPrior counts prior by url
func countURLPrior(s string, conf config) (res int) {

	// parse url
	u, err := url.Parse(s)
	if err != nil {
		log.Println("can not parse url")
		return 0
	}

	// if countUrlEnd unsuccessful
	if res = countUrlEnd(u.EscapedPath(), conf); res == 0 {
		// then counts url queries
		res = countURLQuery(u.Query(), conf)
	}
	// counts domain
	res += countDomain(u.Hostname(), conf)

	return res
}

// countUrlEnd gets url escaped path and config
func countUrlEnd(p string, conf config) (res int) {

	// loop looks for all url ends in config
	for _, end := range conf.Url.UrlEnd {
		// creates regexp
		r := regexp.MustCompile(fmt.Sprintf("(?i)%s$", end.Name))

		// matches regexp with escaped path
		if r.MatchString(p) {
			res = end.Item
			return res
		}
	}

	return 0
}

// countUrlQuery gets url queries and config
func countURLQuery(queries url.Values, conf config) (res int) {

	// adds all queries (map) into slice
	var vals []string
	for _, q := range queries {
		for _, v := range q {
			vals = append(vals, v)
		}
	}

	// loop looks for all url QueryArgs in config
	for _, qArg := range conf.Url.QueryArgs {
		r, err := regexp.Compile(fmt.Sprintf("(?i)%s$", qArg.Name))
		if err != nil {
			log.Printf("%v", err)
		}

		for _, v := range vals {
			if r.MatchString(v) {
				res = qArg.Item
				return res
			}
		}
	}

	return 0
}

// countUrlQuery gets url domain and config
func countDomain(dom string, conf config) (res int) {

	// spliting damain and all subdomains
	splited := strings.Split(dom, ".")
	len := len(splited)

	// loop looks for all url Domains in config
	for _, d := range conf.Url.Dom {
		if d.Level < 1 || d.Level > len {
			log.Printf("domain %s can not contain level %d", d.Name, d.Item)
		}

		// gets 1 subdomain
		s := splited[len-d.Level]

		// to lower case to compare
		d.Name = strings.ToLower(d.Name)
		s = strings.ToLower(s)

		// comparing with config subdomain
		if s == d.Name {
			res = d.Item
			return res
		}
	}

	return 0
}
