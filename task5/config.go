package task5

// this file contains logic for working with rules.yaml

import (
	"fmt"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

const (
	// config file
	RulesFile = "rules.yaml"
)

// Name-Item params
type nameItem struct {
	Name string `yaml:"name"`
	Item int    `yaml:"item"`
}

// Name-Value-Item params
type nameValueItem struct {
	Name  string `yaml:"name"`
	Value string `yaml:"value"`
	Item  int    `yaml:"item"`
}

// Domain params
type domain struct {
	Name  string `yaml:"name"`
	Level int    `yaml:"level"`
	Item  int    `yaml:"item"`
}

// config structure
type config struct {
	Def int `yaml:"defalult"`
	Url struct {
		QueryArgs []nameItem `yaml:"query_args"`
		UrlEnd    []nameItem `yaml:"url_end"`
		Dom       []domain   `yaml:"domain"`
	} `yaml:"url"`
	Info struct {
		Fields []nameValueItem `yaml:"fields"`
	} `yaml:"info"`
}

// parsing config file
func ParseYaml(filename string) (*config, error) {
	// reading config file
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("can not read config file: %v", err)
	}

	var conf config

	// unmarshalling config file
	err = yaml.Unmarshal(dat, &conf)
	if err != nil {
		return nil, fmt.Errorf("error while unmarshalling: %v", err)
	}

	return &conf, nil
}
